package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class ProjectFindByPart extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfbp";
    }

    @Override
    public @NotNull String getDescription() {
        return "Поиск проектов по части названия или описания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        System.out.println("Введите часть названия или описания");
        String part = serviceLocator.getScanner().nextLine();
        Collection<Project> projectCollection = serviceLocator.getProjectService().findByPartNameOrDescription(serviceLocator.getCurrentUser().getId(), part);
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
