package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class ProjectFindAllOrderReadinessStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfarsc";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все проеты в порядке статуса готовности";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        Collection<Project> projectCollection = serviceLocator.getProjectService().findAllInOrderReadinessStatus(serviceLocator.getCurrentUser().getId());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
