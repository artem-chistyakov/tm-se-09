package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uupc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля ";
    }

    @Override
    public void execute() {
        System.out.println("Введите старый пароль");
        if (serviceLocator.getUserService().updatePassword(serviceLocator.getCurrentUser(), serviceLocator.getScanner().nextLine()))
            System.out.println("Пароль изменен");
        else System.out.println("Ошибка изменения пароля");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
