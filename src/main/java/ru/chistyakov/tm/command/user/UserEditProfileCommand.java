package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uepc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение профиля пользователя";
    }

    @Override
    public void execute() {
        System.out.println("Введите новое имя");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println(" Введите новый пароль");
        final String password = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserService().updateUser(serviceLocator.getCurrentUser().getId(), login, password))
            System.out.println("Профиль успешно изменен");
        else System.out.println("Ошибка изменения профиля");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
