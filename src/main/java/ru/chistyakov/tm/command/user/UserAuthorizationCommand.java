package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

public final class UserAuthorizationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

    @Override
    public void execute() {
        System.out.println("введите имя");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("введите пароль");
        final String password = serviceLocator.getScanner().nextLine();
        final User currentUser = serviceLocator.getUserService().authoriseUser(login, password);
        if (currentUser == null) System.out.println("Пользователя с таким логином и паролем не существует");
        else serviceLocator.setCurrentUser(currentUser);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ANONIM};
    }
}
