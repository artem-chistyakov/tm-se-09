package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class UserEndSessionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uesc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Заканчивает пользовательскую сессию";
    }

    @Override
    public void execute() {
        serviceLocator.setCurrentUser(null);
        System.out.println("Пользовательская сессия окончена");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
