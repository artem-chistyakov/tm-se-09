package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class TaskFindAllOrderDateBeginCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tfadbc";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все задачи в порядке даты начала";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        Collection<Task> collection = serviceLocator.getTaskService().findAllInOrderDateBegin(serviceLocator.getCurrentUser().getId());
        if (collection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (Task task : collection) System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
