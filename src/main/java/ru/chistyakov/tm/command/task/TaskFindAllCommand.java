package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tfal";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Найти все команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        final Collection<Task> collection = serviceLocator.getTaskService().findAll(serviceLocator.getCurrentUser().getId());
        if (collection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final Task task : collection) System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
