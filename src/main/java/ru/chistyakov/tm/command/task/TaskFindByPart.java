package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class TaskFindByPart extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tfbp";
    }

    @Override
    public @NotNull String getDescription() {
        return "ищет задачи по части названия или описания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        System.out.println("Введите часть названия или описания");
        String part = serviceLocator.getScanner().nextLine();
        final Collection<Task> taskCollection = serviceLocator.getTaskService().findByPartNameOrDescription(serviceLocator.getCurrentUser().getId(), part);
        if (taskCollection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final Task task : taskCollection) System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
