package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;


public class TaskMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если задачи не существует создает новую, иначе её обновляет";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        System.out.println("Введите id задачи");
        final String idTask = serviceLocator.getScanner().nextLine();
        final Task task = new Task();
        task.setId(idTask);
        if (serviceLocator.getTaskService().merge(task)) System.out.println("Команда выполнена");
        else throw new IllegalArgumentException("Ошибка выполнения команды");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
