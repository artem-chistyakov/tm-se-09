package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.ProjectRepository;
import ru.chistyakov.tm.repository.TaskRepository;
import ru.chistyakov.tm.repository.UserRepository;
import ru.chistyakov.tm.service.ProjectService;
import ru.chistyakov.tm.service.TaskService;
import ru.chistyakov.tm.service.UserService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private User currentUser;
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
    private final Scanner scanner = new Scanner(System.in);
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final static Set<Class<? extends AbstractCommand>> CLASSES = new Reflections("ru.chistyakov.tm.command").getSubTypesOf(AbstractCommand.class);

    @Override
    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

    @NotNull
    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }


    private void registry(@NotNull final AbstractCommand abstractCommand) {
        commandMap.put(abstractCommand.getName(), abstractCommand);
    }

    private void registry(@NotNull final Class clazz) {
        try {
            AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(this);
            registry(command);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes) {
        for (Class clazz : classes) registry(clazz);
    }

    private void execute(@NotNull final AbstractCommand abstractCommand, @Nullable User currentUser)
            throws NullPointerException, IllegalArgumentException, UnsupportedOperationException {
        if (currentUser == null) currentUser = new User();
        final Collection<RoleType> roleTypeCollection = Arrays.asList(abstractCommand.getSupportedRoles());
        if (roleTypeCollection.contains(currentUser.getRoleType())) abstractCommand.execute();
        else throw new UnsupportedOperationException("Вам не доступна данная команда.");
    }

    private void init() {
        registry(Bootstrap.CLASSES);
    }

    public void start() {
        init();
        userService.registryAdmin("admin", "admin");
        userService.registryUser("user", "user");
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            System.out.println("\n" + "Введите команду");
            final AbstractCommand abstractCommand = commandMap.get(scanner.nextLine());
            if (abstractCommand == null) {
                System.out.println("Такой команды не существует");
                continue;
            }
            try {
                execute(abstractCommand, currentUser);
            } catch (NullPointerException | IllegalArgumentException | UnsupportedOperationException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}

