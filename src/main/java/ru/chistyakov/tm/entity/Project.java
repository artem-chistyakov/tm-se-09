package ru.chistyakov.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;

    @Nullable
    private String description;

    @Nullable
    private Date dateBeginProject;

    @Nullable
    private Date dateEndProject;

    @Nullable
    private String userId;

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", readinessStatus=" + readinessStatus +
                ", description='" + description + '\'' +
                ", dateBeginProject=" + dateBeginProject +
                ", dateEndProject=" + dateEndProject +
                ", idUser='" + userId + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}

