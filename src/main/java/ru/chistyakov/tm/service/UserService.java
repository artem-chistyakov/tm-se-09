package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.utility.PasswordParser;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean merge(@NotNull final User user) {
        return userRepository.merge(user);
    }

    @Override
    public boolean persist(@NotNull final User user) {
        return userRepository.persist(user);
    }

    @Override
    @Nullable
    public User authoriseUser(@NotNull final String login, @NotNull final String password) {
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.findUserByLoginAndPassword(login, passwordMD5);
    }

    @Override
    public boolean updateUser(@NotNull final String userId, @NotNull final String login, @NotNull final String password) {
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.update(userId, login, passwordMD5);
    }

    @Override
    public boolean updatePassword(@NotNull final User user, @NotNull final String password) {
        if (password.trim().isEmpty()) return false;
        updateUser(user.getId(), user.getLogin(), password);
        return true;
    }

    @Override
    public boolean registryAdmin(@NotNull final String login, @NotNull final String password) {
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.insertAdmin(login, passwordMD5);
    }

    @Override
    public boolean registryUser(@NotNull final String login, @NotNull final String password) {
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.insert(login, passwordMD5);
    }
}
