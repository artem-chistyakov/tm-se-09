package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.comporator.TaskDateBeginComparator;
import ru.chistyakov.tm.comporator.TaskDateEndComparator;
import ru.chistyakov.tm.comporator.TaskReadinessStatusComparator;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.utility.DateParser;

import java.util.Collection;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;
    private final IProjectRepository projectRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public boolean merge(@Nullable final Task task) {
        if (task == null) return false;
        return taskRepository.merge(task);
    }

    @Override
    public boolean persist(@NotNull final Task task) {
        return taskRepository.persist(task);
    }

    @Override
    public boolean insert(@NotNull final String userId, @NotNull final String projectId,
                          @NotNull final String taskName, @Nullable final String descriptionTask,
                          @Nullable final String dateBeginTask, @Nullable final String dateEndTask) {
        if (userId.trim().isEmpty()) return false;
        if (taskName.trim().isEmpty() || projectId.trim().isEmpty() || projectRepository.findOne(userId, projectId) == null)
            return false;
        return taskRepository.insert(
                userId, taskName, projectId, descriptionTask,
                DateParser.parseDate(simpleDateFormat, dateBeginTask),
                DateParser.parseDate(simpleDateFormat, dateEndTask));

    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return null;
        return taskRepository.findOne(userId, taskId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId,
                          @NotNull final String taskName, @Nullable final String description, @Nullable final String dateBegin,
                          @Nullable final String dateEnd) {
        if (taskId.trim().isEmpty() || taskName.trim().isEmpty()) return false;
        if (projectRepository.findOne(userId, projectId) == null) return false;
        return taskRepository.update(
                userId, taskId, projectId, taskName, description,
                DateParser.parseDate(simpleDateFormat, dateBegin),
                DateParser.parseDate(simpleDateFormat, dateEnd));
    }

    @Override
    public boolean remove(@NotNull final String idUser, @NotNull final String idTask) {
        if (idUser.trim().isEmpty() || idTask.trim().isEmpty()) return false;
        return taskRepository.remove(idUser, idTask);
    }


    @Override
    public void removeAll(@NotNull final String idUser) {
        taskRepository.removeAll(idUser);
    }

    @Override
    public @NotNull Collection<Task> findAllInOrderDateBegin(@NotNull final String idUser) {
        return taskRepository.findAllInOrder(idUser, new TaskDateBeginComparator());
    }

    @Override
    public @NotNull Collection<Task> findAllInOrderDateEnd(@NotNull final String idUser) {
        return taskRepository.findAllInOrder(idUser, new TaskDateEndComparator());
    }

    @Override
    public @NotNull Collection<Task> findAllInOrderReadinessStatus(@NotNull final String idUser) {
        return taskRepository.findAllInOrder(idUser, new TaskReadinessStatusComparator());
    }

    @Override
    public @NotNull Collection<Task> findByPartNameOrDescription(@NotNull final String idUser, @NotNull final String part) {
        return taskRepository.findByPartNameOrDescription(idUser, part);
    }
}
