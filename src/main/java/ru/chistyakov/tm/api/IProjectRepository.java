package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;

public interface IProjectRepository {


    boolean insert(@NotNull String userId, @NotNull String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    boolean persist(@NotNull Project project);

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Collection<Project> findAll(@NotNull String userId);

    @NotNull
    Collection<Project> findAllInOrder(@NotNull String userId, Comparator<Project> comparator);

    boolean update(@NotNull String projectId, @NotNull String projectName, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    boolean remove(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId);

    boolean merge(@NotNull Project project);

    Collection<Project> findByPartNameOrDescription(String userId, String part);
}
