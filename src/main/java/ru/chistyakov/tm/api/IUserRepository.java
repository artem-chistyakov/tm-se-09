package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;

import java.util.Collection;

public interface IUserRepository {

    boolean insert(@NotNull String login, @NotNull String password);

    boolean insertAdmin(@NotNull String login, @NotNull String password);

    @Nullable
    User findOne(@NotNull String id);

    Collection<User> findByLogin(@NotNull String userName);

    boolean remove(@NotNull String id);

    boolean persist(@NotNull User user);

    boolean merge(@NotNull User user);

    boolean update(@NotNull String id, @NotNull String login, @NotNull String password);

    User findUserByLoginAndPassword(@NotNull String login, @NotNull String passwordMD5);
}
