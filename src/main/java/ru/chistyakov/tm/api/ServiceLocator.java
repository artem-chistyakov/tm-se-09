package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.User;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {


    User getCurrentUser();

    void setCurrentUser(@Nullable User currentUser);

    @NotNull
    IUserService getUserService();

    @NotNull
    Map<String, AbstractCommand> getCommandMap();

    @NotNull
    Scanner getScanner();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

}
