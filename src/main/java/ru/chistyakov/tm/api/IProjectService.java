package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;

public interface IProjectService {

    boolean insert(@NotNull String userId, @NotNull String name, @Nullable String descriptionProject, @Nullable String dateBegin, @Nullable String dateEnd);

    boolean merge(@Nullable Project project);

    void removeAll(@NotNull String userId);

    boolean persist(@NotNull Project project);

    boolean remove(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String idProject);

    boolean update(@NotNull String projectId, @NotNull String projectName, @Nullable String descriptionProject, @Nullable String dateBegin, @Nullable String dateEnd);

    @NotNull
    Collection<Project> findAll(String userId);

    @NotNull
    Collection<Project> findAllInOrderDateBegin(@NotNull final String userId);

    @NotNull
    Collection<Project> findAllInOrderDateEnd(@NotNull final String userId);

    @NotNull
    Collection<Project> findAllInOrderReadinessStatus(@NotNull final String userId);

    @NotNull
    Collection<Project> findByPartNameOrDescription(@NotNull final String userId, String part);

}