package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.entity.Task;

import java.util.Comparator;

public final class TaskReadinessStatusComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull final Task o1, @NotNull final Task o2) {
        int resultComparing = o1.getReadinessStatus().compareTo(o2.getReadinessStatus());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}

