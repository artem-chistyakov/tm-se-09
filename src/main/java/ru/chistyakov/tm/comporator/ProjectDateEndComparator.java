package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectDateEndComparator implements Comparator<Project> {
    @Override
    public int compare(@NotNull final Project o1, @NotNull final Project o2) {
        if (o1.getDateEndProject() == null) return -1;
        if (o2.getDateEndProject() == null) return 1;
        int resultComparing = o1.getDateEndProject().compareTo(o2.getDateEndProject());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
