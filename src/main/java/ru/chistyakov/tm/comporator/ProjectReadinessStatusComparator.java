package ru.chistyakov.tm.comporator;

import ru.chistyakov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectReadinessStatusComparator implements Comparator<Project> {
    @Override
    public int compare(final Project o1, final Project o2) {
        int resultComparing = o1.getReadinessStatus().compareTo(o2.getReadinessStatus());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
