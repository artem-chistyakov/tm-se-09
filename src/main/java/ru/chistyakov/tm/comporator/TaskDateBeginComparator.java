package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.entity.Task;

import java.util.Comparator;

public final class TaskDateBeginComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull final Task o1, @NotNull final Task o2) {
        if (o1.getDateBeginTask() == null) return -1;
        if (o2.getDateBeginTask() == null) return 1;
        int resultComparing = o1.getDateBeginTask().compareTo(o2.getDateBeginTask());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
