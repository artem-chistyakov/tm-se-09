package ru.chistyakov.tm.utility;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public final class PasswordParser {

    @Nullable
    public static String parse(@NotNull final String password) {
        try {
            final byte[] bytesOfMessage = password.getBytes(StandardCharsets.UTF_8);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] thedigest = md.digest(bytesOfMessage);
            return new String(thedigest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
