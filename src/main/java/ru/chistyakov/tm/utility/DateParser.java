package ru.chistyakov.tm.utility;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateParser {

    @Nullable
    public static Date parseDate(@NotNull final SimpleDateFormat simpleDateFormat, @Nullable final String date) {
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException parseException) {
            return null;
        }
    }
}
