package ru.chistyakov.tm;

import org.apache.log4j.BasicConfigurator;
import ru.chistyakov.tm.loader.Bootstrap;

public final class App {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}